package com.zopa.loanmachine.offer;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class LoanOfferTest {

    @Test(expected = NullPointerException.class)
    public void test_compare_loan_offers_rates_to_null_throws_exception() {
        LoanOffer offer1 = new LoanOffer("John", BigDecimal.valueOf(0.069), BigDecimal.valueOf(100));
        offer1.compareTo(null);
    }

    @Test(expected = NullPointerException.class)
    public void test_compare_loan_offers_non_null_rates() {
        new LoanOffer("John", null, BigDecimal.valueOf(100));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_compare_loan_offers_non_zero_rates() {
        new LoanOffer("John", BigDecimal.ZERO, BigDecimal.valueOf(100));
    }

    @Test(expected = NullPointerException.class)
    public void test_compare_loan_offers_non_null_amounts() {
        new LoanOffer("John", BigDecimal.valueOf(0.069), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_compare_loan_offers_non_zero_amounts() {
        new LoanOffer("John", BigDecimal.valueOf(0.069), BigDecimal.ZERO);
    }

    @Test
    public void test_compare_loan_offers_equal_rates() {
        LoanOffer offer1 = new LoanOffer("John", BigDecimal.valueOf(0.069), BigDecimal.valueOf(100));
        LoanOffer offer2 = new LoanOffer("Mark", BigDecimal.valueOf(0.069), BigDecimal.valueOf(100));

        assertEquals(0, offer1.compareTo(offer2));
        assertEquals(0, offer2.compareTo(offer1));
    }

    @Test
    public void test_compare_loan_offers_different_rates() {
        LoanOffer offer1 = new LoanOffer("John", BigDecimal.valueOf(0.069), BigDecimal.valueOf(100));
        LoanOffer offer2 = new LoanOffer("Mark", BigDecimal.valueOf(0.075), BigDecimal.valueOf(100));

        assertEquals(-1, offer1.compareTo(offer2));
        assertEquals(1, offer2.compareTo(offer1));
    }

    @Test
    public void test_compare_loan_offers_equal_rates_different_amounts() {
        LoanOffer offer1 = new LoanOffer("John", BigDecimal.valueOf(0.069), BigDecimal.valueOf(400));
        LoanOffer offer2 = new LoanOffer("Mark", BigDecimal.valueOf(0.069), BigDecimal.valueOf(100));

        assertEquals(-1, offer1.compareTo(offer2));
        assertEquals(1, offer2.compareTo(offer1));
    }
}
