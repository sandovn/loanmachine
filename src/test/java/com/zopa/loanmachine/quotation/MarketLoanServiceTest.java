package com.zopa.loanmachine.quotation;

import com.zopa.loanmachine.exception.InsufficientFundsException;
import com.zopa.loanmachine.offer.LoanOffer;
import com.zopa.loanmachine.offer.RateOffer;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MarketLoanServiceTest {

    private static final int LOAN_TERM_IN_MONTHS = 36;

    private static QuoteService quoteService;

    @Before
    public void setUp() {
        List<LoanOffer> marketOffers = Arrays.asList(
                new LoanOffer("Bob", BigDecimal.valueOf(0.075), BigDecimal.valueOf(640)),
                new LoanOffer("Jane", BigDecimal.valueOf(0.069), BigDecimal.valueOf(480)),
                new LoanOffer("Fred", BigDecimal.valueOf(0.071), BigDecimal.valueOf(520)),
                new LoanOffer("Mary", BigDecimal.valueOf(0.104), BigDecimal.valueOf(170)),
                new LoanOffer("John", BigDecimal.valueOf(0.081), BigDecimal.valueOf(320)),
                new LoanOffer("Dave", BigDecimal.valueOf(0.074), BigDecimal.valueOf(140)),
                new LoanOffer("Angela", BigDecimal.valueOf(0.071), BigDecimal.valueOf(60))
        );

        quoteService = new MarketLoanService(marketOffers);
    }

    @Test(expected = NullPointerException.class)
    public void test_invalid_loan_amount_of_null() throws Exception {
        quoteService.calculateRate(null, LOAN_TERM_IN_MONTHS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_invalid_loan_amount_too_low() throws Exception {
        quoteService.calculateRate(BigDecimal.valueOf(999), LOAN_TERM_IN_MONTHS);
    }

    @Test
    public void test_valid_loan_amount_of_one_thousand() throws Exception {
        quoteService.calculateRate(BigDecimal.valueOf(1000), LOAN_TERM_IN_MONTHS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_invalid_loan_amount_too_high() throws Exception {
        quoteService.calculateRate(BigDecimal.valueOf(15001), LOAN_TERM_IN_MONTHS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_invalid_loan_amount_of_non_hundred_increment() throws Exception {
        quoteService.calculateRate(BigDecimal.valueOf(199), LOAN_TERM_IN_MONTHS);
    }

    @Test(expected = InsufficientFundsException.class)
    public void test_loan_amount_larger_than_market_offering() throws Exception {
        BigDecimal requestAmount = BigDecimal.valueOf(2400);
        quoteService.calculateRate(requestAmount, LOAN_TERM_IN_MONTHS);
    }

    @Test
    public void test_valid_loan_amount_and_compound_interest() throws Exception {
        BigDecimal requestAmount = BigDecimal.valueOf(1000);
        RateOffer offer = quoteService.calculateRate(requestAmount, LOAN_TERM_IN_MONTHS);

        assertNotNull(offer);
        assertEquals(requestAmount, offer.getRequestedAmount());
        assertEquals(BigDecimal.valueOf(7).setScale(1, BigDecimal.ROUND_HALF_UP), offer.getRate());
        assertEquals(BigDecimal.valueOf(30.78).setScale(2, BigDecimal.ROUND_HALF_UP), offer.getMonthlyRepayment());
        assertEquals(BigDecimal.valueOf(1108.08).setScale(2, BigDecimal.ROUND_HALF_UP), offer.getTotalRepayment());
    }

    @Test
    public void test_valid_loan_amount_and_compound_interest_with_roundup() throws Exception {
        BigDecimal requestAmount = BigDecimal.valueOf(2000);
        RateOffer offer = quoteService.calculateRate(requestAmount, LOAN_TERM_IN_MONTHS);

        assertNotNull(offer);
        assertEquals(requestAmount, offer.getRequestedAmount());
        // rate is officially 0.0735 though it's rounded up up to 1 scale
        assertEquals(BigDecimal.valueOf(7.4).setScale(1, BigDecimal.ROUND_HALF_UP), offer.getRate());
        assertEquals(BigDecimal.valueOf(61.86).setScale(2, BigDecimal.ROUND_HALF_UP), offer.getMonthlyRepayment());
        assertEquals(BigDecimal.valueOf(2226.96).setScale(2, BigDecimal.ROUND_HALF_UP), offer.getTotalRepayment());
    }

    @Test
    public void test_valid_loan_amount_and_compound_for_very_large_market() throws Exception {
        BigDecimal amount = BigDecimal.valueOf(15);
        BigDecimal rate = BigDecimal.valueOf(0.001);
        List<LoanOffer> marketOffers = new ArrayList<>();
        for (int i=0; i < 5000; i++) {
            marketOffers.add(
                    new LoanOffer("Bob", rate, amount)
            );
            rate = rate.add(BigDecimal.valueOf(0.001));
        }

        BigDecimal requestAmount = BigDecimal.valueOf(15000);
        MarketLoanService quoteService = new MarketLoanService(marketOffers);
        RateOffer offer = quoteService.calculateRate(requestAmount, LOAN_TERM_IN_MONTHS);

        assertNotNull(offer);
        assertEquals(requestAmount, offer.getRequestedAmount());
        assertEquals(BigDecimal.valueOf(50.1).setScale(1, BigDecimal.ROUND_HALF_UP), offer.getRate());
        assertEquals(BigDecimal.valueOf(732.84).setScale(2, BigDecimal.ROUND_HALF_UP), offer.getMonthlyRepayment());
        assertEquals(BigDecimal.valueOf(26382.24).setScale(2, BigDecimal.ROUND_HALF_UP), offer.getTotalRepayment());
    }
}
