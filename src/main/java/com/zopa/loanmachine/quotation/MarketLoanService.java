package com.zopa.loanmachine.quotation;

import com.zopa.loanmachine.exception.InsufficientFundsException;
import com.zopa.loanmachine.offer.LoanOffer;
import com.zopa.loanmachine.offer.RateOffer;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Market loan service that calculates the offer in memory
 * using predefined list of loan offers.
 */
public class MarketLoanService implements QuoteService {

    private static final int LOAN_INCREMENT_STEP = 100;

    /**
     * Minimum amount for any given loan request.
     * This could be transformed into constructor
     * parameter and configured with DI.
     */
    private static final int MIN_LOAN_AMOUNT = 1000;

    /**
     * Maximum amount for any given loan request.
     * This could be transformed into constructor
     * parameter and configured with DI.
     */
    private static final int MAX_LOAN_AMOUNT = 15000;

    /**
     * This are all of the data offers from various clients
     * holding the name of the customer, amount and rate.
     */
    private List<LoanOffer> marketOffers;

    /**
     * On instantiation we need to first sort the data offers
     * and after that we need to set them. This on average would
     * take O(n log(n)) though it will make sure that we get correct
     * results for our rate/amount calculation further down.
     * @param marketOffers data offers
     */
    public MarketLoanService(@NotNull List<LoanOffer> marketOffers) {
        checkNotNull(marketOffers);
        Collections.sort(marketOffers);
        this.marketOffers = Collections.unmodifiableList(marketOffers);
    }

    @Override
    public RateOffer calculateRate(
            @NotNull BigDecimal loanAmount,
            int loanTermInMonths
    ) throws IllegalArgumentException, InsufficientFundsException {

        checkNotNull(loanAmount);
        checkArgument(
                loanAmount.remainder(BigDecimal.valueOf(LOAN_INCREMENT_STEP)).compareTo(BigDecimal.ZERO) == 0,
                "Loan amount has to be multiple of " + LOAN_INCREMENT_STEP
        );
        checkArgument(
                loanAmount.compareTo(BigDecimal.valueOf(MIN_LOAN_AMOUNT)) >= 0,
                "Loan amount has to be larger or equal to " + MIN_LOAN_AMOUNT
        );
        checkArgument(
                loanAmount.compareTo(BigDecimal.valueOf(MAX_LOAN_AMOUNT)) <= 0,
                "Loan amount has to be smaller or equal to " + MAX_LOAN_AMOUNT
        );

        BigDecimal availableAmount = BigDecimal.ZERO;
        BigDecimal totalInterestRate = BigDecimal.ZERO;
        int totalCountOfRates = 0;
        for (LoanOffer offer : marketOffers) {
            if (availableAmount.compareTo(loanAmount) < 0) {
                availableAmount = availableAmount.add(offer.getAmount());
                totalInterestRate = totalInterestRate.add(offer.getRate());
                totalCountOfRates++;
            } else {
                break;
            }
        }

        // check if we have enough funds to cover the requested loan
        if (availableAmount.compareTo(loanAmount) < 0) {
            throw new InsufficientFundsException("There are not enough funds to cover a loan of \""+loanAmount+"\"");
        }

        BigDecimal averageInterestRate = totalInterestRate.divide(
                BigDecimal.valueOf(totalCountOfRates), MathContext.DECIMAL64
        ).multiply(BigDecimal.valueOf(100));

        BigDecimal monthlyPayment = calculateMonthlyInstalment(
                loanAmount, averageInterestRate.doubleValue(), loanTermInMonths
        );
        BigDecimal totalRepayment = monthlyPayment.multiply(BigDecimal.valueOf(loanTermInMonths));

        return new RateOffer(
                loanAmount,
                averageInterestRate.setScale(1, BigDecimal.ROUND_HALF_UP),
                monthlyPayment,
                totalRepayment
        );
    }

    /**
     * Given an effective compound rate for 12 months convert it to
     * nominal rate so we could calculate the correct monthly instalments
     * for the loan.
     *
     * Using the following formula:
     *     ( (1 + R)^(1/k) - 1 ) * k
     *     k - number of compounds per year, ex: 12
     *     R - effective annual rate (compounded)
     *
     * @return nominal compound rate
     */
    private double convertEffectiveToNominalRate(double effectiveRate, int compoundsPerYear) {

        double decimalRate = effectiveRate / 100.0;
        return (Math.pow(1 + decimalRate, 1.0 / compoundsPerYear) - 1) * compoundsPerYear;
    }

    /**
     * Calculate monthly instalment for a given effective interest rate.
     *
     * Using the following formula:
     *     (L * R) / (1 - (1 / (monthlyRate + 1)^termInMonths ))
     *     L - original loan amount
     *     R - nominal monthly rate (r / 12)
     *
     * @return calculated monthly instalment
     */
    private BigDecimal calculateMonthlyInstalment(
            BigDecimal loanAmount,
            double effectiveInterestRate,
            int termInMonths
    ) {

        // invert the sign as we have debt that's owed
        loanAmount = loanAmount.negate();
        int compoundsPerYear = 12;
        double interestRate = convertEffectiveToNominalRate(effectiveInterestRate, compoundsPerYear);
        BigDecimal monthlyRate = BigDecimal.valueOf(interestRate / compoundsPerYear);

        BigDecimal raisedValue = BigDecimal.ONE.divide(
                monthlyRate.add(BigDecimal.ONE).pow(termInMonths), MathContext.DECIMAL64
        );

        // Calculate the monthly payment
        BigDecimal monthlyPayment = loanAmount.multiply(monthlyRate).divide(
                raisedValue.subtract(BigDecimal.ONE), MathContext.DECIMAL64
        );

        return monthlyPayment.setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
