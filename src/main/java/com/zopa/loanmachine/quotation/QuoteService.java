package com.zopa.loanmachine.quotation;

import com.zopa.loanmachine.exception.InsufficientFundsException;
import com.zopa.loanmachine.offer.RateOffer;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public interface QuoteService {

    /**
     * Calculate loan offer and rate for given amount against
     * given data offers
     * @param loanAmount the amount to be borrowed
     * @param loanTerm the term (months/years) the loan is to be borrowed
     * @return compound rate offer
     * @throws IllegalArgumentException if the loan amount is invalid
     * @throws InsufficientFundsException if the data doesn't have enough
     *                                    funds to cover the loan
     */
    RateOffer calculateRate(
            BigDecimal loanAmount,
            int loanTerm
    ) throws IllegalArgumentException, InsufficientFundsException;
}
