package com.zopa.loanmachine.config;

import com.zopa.loanmachine.data.DataLoader;
import com.zopa.loanmachine.data.MarketCsvDataLoader;
import com.zopa.loanmachine.offer.LoanOffer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public DataLoader marketCsvDataLoader() {
        return new MarketCsvDataLoader(LoanOffer.class);
    }
}
