package com.zopa.loanmachine;

import com.zopa.loanmachine.data.DataLoader;
import com.zopa.loanmachine.exception.InsufficientFundsException;
import com.zopa.loanmachine.offer.LoanOffer;
import com.zopa.loanmachine.offer.RateOffer;
import com.zopa.loanmachine.quotation.MarketLoanService;
import com.zopa.loanmachine.quotation.QuoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.List;

/**
 * This is out entry point for the application.
 * All of the web support has been disabled as this
 * was expected to be a console application.
 *
 * Another approach could be to accept requests on localhost:8080
 * and serve back as normal web application in RESTful manner.
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(Application.class);

    private static final int LOAN_TERM_IN_MONTHS = 36;

    private DataLoader dataLoader;

    @Autowired
    public Application(DataLoader dataLoader) {
        this.dataLoader = dataLoader;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Starting loan and rate calculation");

        validateArgs(args);

        String filePath = args[0];
        String loanAmount = args[1];

        long startTime = System.nanoTime();

        List<LoanOffer> marketData = dataLoader.load(filePath);
        QuoteService quotationService = new MarketLoanService(marketData);

        RateOffer offer = null;
        try {
            offer = quotationService.calculateRate(new BigDecimal(loanAmount), LOAN_TERM_IN_MONTHS);
        } catch (InsufficientFundsException e) {
            renderMessage(e.getMessage());
            System.exit(1);
        } catch (IllegalArgumentException e) {
            renderMessage(e.getMessage());
            System.exit(2);
        }

        long endTime = System.nanoTime();
        logger.info("Execution time milliseconds end: " + (endTime - startTime) / 1000000);

        renderMessage(
                "Requested amount: £" + offer.getRequestedAmount(),
                "Rate: " + offer.getRate() + "%",
                "Monthly repayment: £" + offer.getMonthlyRepayment(),
                "Total repayment: £" + offer.getTotalRepayment()
        );
    }

    private void validateArgs(String[] args) {
        if (args.length != 2) {
            renderMessage(
                "The application expects exactly two arguments:",
                "1) Path to CSV file with data loan offers",
                "2) Requested loan amount between 1000 and 15000"
            );
            System.exit(3);
        }
    }

    private void renderMessage(String... messages) {
        logger.info("\n\n\n--------------------------------\n");
        for (String message : messages) {
            logger.info(message);
        }
        logger.info("\n\n\n--------------------------------\n");
    }
}
