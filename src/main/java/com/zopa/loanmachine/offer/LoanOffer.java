package com.zopa.loanmachine.offer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class LoanOffer implements Comparable<LoanOffer> {

    private String lenderName;
    private BigDecimal rate;
    private BigDecimal amount;

    /**
     * This is an entity that represents a Loan offer from a lender.
     * The amount and rate are always required and must be greater than zero.
     * @param lenderName lender name
     * @param rate loan rate
     * @param amount loan amount
     */
    @JsonCreator
    public LoanOffer(
            @NotNull @JsonProperty("Lender")String lenderName,
            @NotNull @JsonProperty("Rate") BigDecimal rate,
            @NotNull @JsonProperty("Available") BigDecimal amount
    ) {
        checkNotNull(lenderName, "Lender name cannot be null.");
        checkNotNull(rate, "Loan rate cannot be null.");
        checkArgument(rate.compareTo(BigDecimal.ZERO) > 0, "Loan rate must be greater than 0.");
        checkNotNull(amount, "Loan amount cannot be null.");
        checkArgument(amount.compareTo(BigDecimal.ZERO) > 0, "Loan amount must be greater than 0.");

        this.lenderName = lenderName;
        this.rate = rate;
        this.amount = amount;
    }

    public String getLenderName() {
        return lenderName;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * We need a custom comparison, especially for cases where
     * loan offer rates are equal we need to check the amount
     * and the offer that has higher amount should be treated
     * as smaller and go before the other offer. This is to make
     * sure that we always use the lowest rate with biggest amount available.
     * @param lo loan offer to compare against
     */
    @Override
    public int compareTo(LoanOffer lo) {
        if (rate.equals(lo.getRate())) {
            return -1 * amount.compareTo(lo.getAmount());
        }

        return rate.compareTo(lo.getRate());
    }

    @Override
    public boolean equals(Object lo) {
        if (this == lo) return true;
        if (!(lo instanceof LoanOffer)) return false;
        LoanOffer loanOffer = (LoanOffer) lo;
        return Objects.equals(getRate(), loanOffer.getRate()) &&
                Objects.equals(getAmount(), loanOffer.getAmount());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getRate(), getAmount());
    }
}
