package com.zopa.loanmachine.exception;

public class InsufficientFundsException extends Exception {

    /**
     * Default constructor
     */
    public InsufficientFundsException() {
        super();
    }

    public InsufficientFundsException(String message) {
        super(message);
    }
}
