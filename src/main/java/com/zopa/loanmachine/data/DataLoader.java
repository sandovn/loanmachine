package com.zopa.loanmachine.data;

import java.util.List;

/**
 * Generic data loader for market loan offers.
 */
public interface DataLoader {

    /**
     * Load market offers' data from various data sources
     * for given path.
     * @return data list
     * @throws Exception if data cannot be loaded
     */
    <T> List<T> load(String path) throws Exception;
}
