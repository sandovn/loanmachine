package com.zopa.loanmachine.data;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MarketCsvDataLoader implements DataLoader {

    private static final Logger logger = LoggerFactory.getLogger(MarketCsvDataLoader.class);

    private Class type;

    public MarketCsvDataLoader(Class type) {
        this.type = type;
    }

    @Override
    public <T> List<T> load(String path) throws IOException {
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        CsvMapper mapper = new CsvMapper();
        File file = new File(path);
        MappingIterator<T> readValues = mapper.readerWithSchemaFor(type).with(schema).readValues(file);
        return readValues.readAll();
    }
}
