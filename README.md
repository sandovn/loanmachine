Loan Quoter Service
===

This is a Java service that allows the generation of a quite for given 
input market offers and a required quote from a client. There are certain 
limitations to the actual service:

- Min loan value: £1000
- Max loan value: £15000
- Increment step: £100

The service expects market offers to be passed with each request in CSV format 
with following columns: "Lender", "Rate", "Available". If data is not in correct
format an exception will be thrown with relevant message.

The application runs in console for that reason all of the web functionalitites
of Spring have been disabled. You can see build and run steps below.

Another approach on building the service is allow it to run in embedded Tomcat 
container on localhost:8080 and implement the service in RESTful fashion where
you can do requests again the URL and receive JSON responses.

The monthly and total rates are generated based on nominal rate that is being 
generated from a given annual effective rate. The next part is the amortised 
monthly payments that are based on 36 months and the loan amount entered by the
user of the service. The actual numbers will be presented in required format in 
the console. 

### Performance 

Few performance tests were run against the service with different inputs. 
The average load of the application is about ~80 milliseconds for loan quote.
All of the operations are done in memory to speedup the process. This decision was
based on the fact that we do only one calculation for any given set of data and loan 
request.

If needed to do multiple quotes over the same set of data some data persistence layer
could be used in form of database (relational or NoSQL) but that may have an impact 
on the general performance.   

Requirements
---
- Java 8

Build
---
```
# mvn clean package
```

Run project
---
```
# java -jar command-line.jar <FILE_PATH_MARKET_CSV> <LOAN_AMOUNT>
```

Example:
```
# java -jar target/command-line.jar ./market_data.csv 1000
```
